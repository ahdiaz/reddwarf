/**
 * RedDwarf
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#ifndef __RED_DWARF_DATETIME_H__
#define __RED_DWARF_DATETIME_H__

#include <time.h>

#define DATE_LEN               30
#define DATE_FORMAT_RFC2616    "%a, %d %b %Y %X GMT"
#define DATE_FORMAT_LOG        "%d/%b/%Y:%X %z"

/**
 * Returns a representation of the time t_time according
 * to rfc2616: http://www.w3.org/Protocols/rfc2616/rfc2616-sec3.html#sec3.3
 * ex: Sun, 06 Nov 1994 08:49:37 GMT
 */
char *
time_to_rfc2616(char *dest, const time_t when);

time_t
rfc2616_to_time(char *date);

char *
time_to_log(char *dest, const time_t when);

int
datecmp(time_t date1, time_t date2);

int
datestrcmp(char *date1, char *date2);

#endif
