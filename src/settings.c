/**
 * RedDwarf
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#include <stdio.h>
#include <string.h>
#include <aul.h>

#include "debug.h"
#include "config.h"
#include "settings.h"


#define SETTINGS_DEFAULT_ADDRESS    "127.0.0.1"
#define SETTINGS_DEFAULT_PORT       8080
#define SETTINGS_DEFAULT_ROOT       "./"
#define SETTINGS_DEFAULT_INDEX      "index.html"
#define SETTINGS_DEFAULT_LISTING    1


// Static instance of the configuration
static Settings settings;


void
settings_check_missing_argument(char *argv[], int item)
{
    if (argv[item + 1] == NULL || argv[item + 1][0] == '-') {
        LOG_ERROR("Missing argument for %s option.\n", argv[item]);
        exit(1);
    }
}

/**
 * Reads the configuration from the input params
 *
 * @param  argc Number of arguments
 * @param  argv The arguments
 * @return      The instance of the configuration object
 */
int
settings_parse_arguments(int argc, char *argv[])
{
    int i;

    for (i = 1; i < argc; i++) {

        if (argv[i][0] != '-') {
            continue;
        }

        if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0) {
            settings_print_help();
            exit(0);
        }

        if (strcmp(argv[i], "-v") == 0 || strcmp(argv[i], "--version") == 0) {
            settings_print_version();
            exit(0);
        }

        if (strcmp(argv[i], "-a") == 0 || strcmp(argv[i], "--address") == 0) {

            settings_check_missing_argument(argv, i);
            str_set(settings.address, argv[++i]);

        } else if (strcmp(argv[i], "-p") == 0 || strcmp(argv[i], "--port") == 0) {

            settings_check_missing_argument(argv, i);
            settings.port = (unsigned int) strtol(argv[++i], (char**) NULL, 10);

        } else if (strcmp(argv[i], "-r") == 0 || strcmp(argv[i], "--root") == 0) {

            settings_check_missing_argument(argv, i);
            str_set(settings.wwwroot, argv[++i]);

        } else if (strcmp(argv[i], "-i") == 0 || strcmp(argv[i], "--index") == 0) {

            settings_check_missing_argument(argv, i);
            str_set(settings.index, argv[++i]);

        } else if (strcmp(argv[i], "--no-listings") == 0) {

            settings.listings = 0;
        }
    }

    return 0;
}

/**
 * Initializes a new settings structure
 */
Settings
settings_init(int argc, char *argv[])
{
    settings.address = str_new_set((char *) SETTINGS_DEFAULT_ADDRESS);
    settings.port = SETTINGS_DEFAULT_PORT;
    settings.wwwroot = str_new_set((char *) SETTINGS_DEFAULT_ROOT);
    settings.index = str_new_set((char *) SETTINGS_DEFAULT_INDEX);
    settings.listings = SETTINGS_DEFAULT_LISTING;

    settings_parse_arguments(argc, argv);

    settings_debug();

    return settings;
}

/**
 * Returns the settings instance, previously
 * initialized by a call to settings_init()
 */
Settings
settings_get()
{
    return settings;
}

/**
 * Display command line help
 */
void
settings_print_help()
{

    // -f --file:\t\tPath to a configuration file\n

    LOG_PRINT("\n\
Usage: "PACKAGE" [options]\n\n\
    -a --address:\tIP address to bind to\n\
    -p --port:\t\tPort to listen on\n\
    -r --root:\t\tPath to the home directory\n\
    -i --index:\t\tDefault index file\n\
    --no-listings:\tDon't return directory listings\n\
    -h --help:\t\tDisplays this help and exit\n\
    -v --version:\tDisplays the program version and exit\n\
\n\
");
}

/**
 * Display program version
 */
void
settings_print_version()
{
    LOG_PRINT(PACKAGE_STRING"\n"PACKAGE_MAINTAINER);
}

/**
 * Prints the current configuration
 */
void
settings_debug()
{
    LOG_DEBUG("===== BEGING SETTINGS =====");
    LOG_DEBUG("address:\t%s", settings.address->str);
    LOG_DEBUG("port:\t%d", settings.port);
    LOG_DEBUG("wwwroot:\t%s", settings.wwwroot->str);
    LOG_DEBUG("index:\t%s", settings.index->str);
    LOG_DEBUG("listings:\t%d", settings.listings);
    LOG_DEBUG("===== END SETTINGS =====");
}