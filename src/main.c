/**
 * RedDwarf
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#include <stdio.h>

#include "debug.h"
#include "http-headers.h"
#include "datetime.h"
#include "settings.h"
#include "socket.h"
#include "http-request.h"
#include "http-response.h"
#include "http.h"


char *
_parse_header(char *header)
{
    if (header == NULL) {
        return "";
    } else {
        return header;
    }
}

/**
 * Handles an HTTP request.
 *
 * @param  char* request
 *         The plain request.
 *
 * @return char*
 */
size_t
http_request_handler(ConnInfo conninfo, char *input, char **output)
{
    unsigned int error;
    HttpRequest *request = NULL;
    HttpResponse *response = NULL;
    String *resp = NULL;
    time_t now_t;
    char buff[DATE_LEN];
    char *now = NULL;

    request = http_request_new();
    request = http_request_parse(request, input, &error);

    http_process_request(request, &response, settings_get());

    resp = http_response_to_string(request, response);

    *output = str_value(resp);
    size_t output_size = str_len(resp);

    now_t = time(&now_t);
    now = time_to_log(buff, now_t);

    LOG_PRINT(
        "%s - [%s] \"%s %s %s\" %d %ld \"%s\" \"%s\"",
        conninfo.client_addr_str,
        now,
        http_request_get_method(request),
        http_request_get_uri(request),
        http_request_get_protocol(request),
        http_response_get_status_code(response),
        output_size,
        _parse_header(http_request_get_header(request, HTTP_HEADER_REFERER)),
        _parse_header(http_request_get_header(request, HTTP_HEADER_USER_AGENT))
    );

    http_request_free(request);
    http_response_free(response);
    str_free(&resp);

    return output_size;
}

/**
 * Main function
 *
 * @param  int argc
 * @param  char* argv
 * @return int
 */
int
main(int argc, char *argv[])
{
    settings_init(argc, argv);

    // LOG_PRINT("\n*******************");
    // LOG_PRINT("*  "PACKAGE_STRING"  *");
    // LOG_PRINT("*******************");

    LOG_INFO(PACKAGE_STRING);

    socket_listen(http_request_handler, settings_get());

    return 0;
}
