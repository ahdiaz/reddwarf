/**
 * RedDwarf
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#ifndef __RED_DWARF_DEBUG_H__
#define __RED_DWARF_DEBUG_H__

#include <stdio.h>

#include "config.h"

#ifdef DEBUG_ENABLED
#define LOG_DEBUG(msg, args...) fprintf(stdout, "DEBUG: %s:%d:%s(): " msg "\n", \
    __FILE__, __LINE__, __func__, ## args)
#else
#define LOG_DEBUG(msg, args...) /* Don't do anything in release builds */
#endif

#define LOG_INFO(msg, args...) fprintf(stdout, "INFO: " msg "\n", ## args)

#define LOG_WARN(msg, args...) fprintf(stdout, "WARN: " msg "\n", ## args)

#define LOG_ERROR(msg, args...) fprintf(stdout, "ERROR: " msg "\n", ## args)

#define LOG_PRINT(msg, args...) fprintf(stdout, msg "\n", ## args)

#endif
