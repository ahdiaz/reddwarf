/**
 * RedDwarf
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <aul.h>

#include "debug.h"
#include "http-request.h"


// Allowed methods
static const char *allowed_methods[] = {
    "GET",
    "HEAD",
    "OPTIONS",
    NULL
};


/**
 * Initializes a request structure
 */
HttpRequest *
http_request_new()
{
    HttpRequest *req = malloc(sizeof(HttpRequest));

    if (req == NULL) {
        return NULL;
    }

    req->headers = hash_new();
    req->protocol = str_new();
    req->method = str_new();
    req->uri = str_new();
    req->content = str_new();

    return req;
}

/**
 * Destroys a request structure
 */
void
http_request_free(HttpRequest *req)
{
    if (req == NULL) {
        return;
    }

    hash_free(&req->headers);
    str_free(&req->protocol);
    str_free(&req->method);
    str_free(&req->uri);
    str_free(&req->content);

    free(req);
    req = NULL;
}

/**
 * Parses the status line, storing the components
 * in the request structure.
 */
void
http_request_parse_status_line(char *line, HttpRequest *req)
{
    char *str = strdup(line);
    char *token;
    int count = 0;

    while ((token = strsep(&str, " ")) != NULL) {

        switch (count) {
            case 0:
                str_set(req->method, token);
                break;
            case 1:
                str_set(req->uri, token);
                break;
            case 2:
                str_set(req->protocol, token);
                break;
        }

        count++;
    }
}

/**
 * Parses a header line, storing it as a key-value
 * pair in the request structure.
 */
void
http_request_parse_header_line(char *line, HttpRequest *req)
{
    char *pos = strchr(line, ':');
    char *key, *value;

    if (pos != NULL) {

        key = malloc((pos - line) * sizeof(char) + 1);
        memset(key, 0, pos - line + 1);
        strncpy(key, line, pos - line);

        // Increment by 2 becasuse of a space.
        // TODO: What happens if there is no such a space?
        // TODO: Trim the value?
        pos += 2;
        value = malloc(strlen(pos) * sizeof(char) + 1);
        memset(value, 0, strlen(pos) + 1);
        strncpy(value, pos, strlen(pos));

        hash_put(req->headers, key, value, sizeof(Hash*), NULL);
    }
}

/**
 * Process a raw content and extracts the different
 * components of the request.
 *
 * @return Returns a RequestErrors value
 */
HttpRequest *
http_request_parse(HttpRequest *req, char *raw, unsigned int *error)
{
    if (req == NULL) {
        *error = HTTP_REQUEST_ERROR_PARSING;
        req->error = *error;
        return NULL;
    }

    char new_line_delim[] = "\r\n";
    char *line;

    *error = HTTP_REQUEST_ERROR_NONE;

    // Pointer to a parser function.
    typedef void (*LineParser)(char*, HttpRequest*);

    // The first line is parsed as a status request,
    // the rest as headers.
    LineParser lineParser = &http_request_parse_status_line;

    req->content = str_set(req->content, raw);
    char *str = strdup(raw);

    while ((line = strsep(&str, new_line_delim)) != NULL) {

        lineParser(line, req);
        lineParser = &http_request_parse_header_line;
    }

    http_request_debug(req);


    char *method = http_request_get_method(req);
    int i = 0, method_is_allowed = 0;

    do {
        method_is_allowed = strcmp(method, allowed_methods[i]) == 0;
        i++;
    } while (method_is_allowed == 0 && allowed_methods[i] != NULL);


    if (method_is_allowed == 0) {

        *error = HTTP_REQUEST_ERROR_METHOD;

    } else if (strcmp(http_request_get_uri(req), "") == 0) {

        *error = HTTP_REQUEST_ERROR_URI;

    } else if (strcmp(http_request_get_protocol(req), "") == 0) {

        *error = HTTP_REQUEST_ERROR_PROTOCOL;
    }

    req->error = *error;

    return req;
}

/**
 * Process a raw content and fills the Request structure
 * with the different components of the request.
 *
 * @return Request
 */
HttpRequest *
http_request_parse_from_string(HttpRequest *req, String *raw, unsigned int *error)
{
    return http_request_parse(req, str_value(raw), error);
}

/**
 * Returns the protocol
 */
char *
http_request_get_protocol(HttpRequest *req)
{
    if (req == NULL) {
        return NULL;
    }

    return str_value(req->protocol);
}

/**
 * Returns the method
 */
char *
http_request_get_method(HttpRequest *req)
{
    if (req == NULL) {
        return NULL;
    }

    return str_value(req->method);
}

/**
 * Returns the requested resource
 */
char *
http_request_get_uri(HttpRequest *req)
{
    if (req == NULL) {
        return NULL;
    }

    return str_value(req->uri);
}

/**
 * Returns the raw request content
 */
char *
http_request_get_raw(HttpRequest *req)
{
    if (req == NULL) {
        return NULL;
    }

    return str_value(req->content);
}

/**
 * Returns the number of headers
 */
size_t
http_request_headers_count(HttpRequest *req)
{
    if (req == NULL) {
        return 0;
    }

    return req->headers->len;
}

/**
 * Returns a header value
 */
char *
http_request_get_header(HttpRequest *req, const char *key)
{
    if (req == NULL) {
        return NULL;
    }

    Node *header = hash_get(req->headers, key);
    char *data = NULL;

    if (header) {
        data = (char*) node_value(header);
    }

    return data;
}

/**
 * Returns the parse error
 */
unsigned int
http_request_get_parse_error(HttpRequest *req)
{
    if (req == NULL) {
        return -1;
    }
    return req->error;
}

int
http_request_headers_iterator(Node *node, Hash *hash, Hash *new_hash, void *user_data)
{
    LOG_DEBUG("%s: %s", node->key, (char *)node->data);

    return 0;
}

/**
 * Prints the parsed request
 */
void
http_request_debug(HttpRequest *req)
{
    if (req == NULL) {
        return;
    }

    LOG_DEBUG("==== BEGIN REQUEST ====");
    LOG_DEBUG("%s %s %s", http_request_get_method(req), http_request_get_uri(req), http_request_get_protocol(req));

    hash_foreach(req->headers, http_request_headers_iterator, NULL);

    LOG_DEBUG("\n---- RAW ----\n%s---- RAW ----\n", str_value(req->content));
    LOG_DEBUG("==== END REQUEST ====");
}
