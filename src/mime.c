/**
 * RedDwarf
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#include <stdio.h>
#include <string.h>
#include <aul.h>

#include "debug.h"
#include "mime.h"


#define MIME_DEFAULT    "text/plain"

// Default mimetype mappings - make sure this array is NULL terminated.
static const char *default_extension_map[] = {
    "application/ogg"      " ogg",
    "application/pdf"      " pdf",
    "application/xml"      " xsl xml",
    "application/xml-dtd"  " dtd",
    "application/xslt+xml" " xslt",
    "application/zip"      " zip",
    "application/json"     " json",
    "audio/mpeg"           " mp2 mp3 mpga",
    "image/gif"            " gif",
    "image/jpeg"           " jpeg jpe jpg",
    "image/png"            " png",
    "image/x-icon"         " ico",
    "text/css"             " css",
    "text/html"            " html htm",
    "text/javascript"      " js",
    "text/plain"           " txt asc",
    "video/mpeg"           " mpeg mpe mpg",
    "video/quicktime"      " qt mov",
    "video/x-msvideo"      " avi",
    NULL
};


char *
mime_get_from_name(char *filename)
{
    if (filename == NULL) {
        return NULL;
    }

    static Hash *mime_map;
    char *mime;

    if (mime_map == NULL) {

        LOG_DEBUG("[Mime]: Initializing mime map");

        mime_map = hash_new();

        for (int i = 0; default_extension_map[i] != NULL; i++) {

            char *str = strdup(default_extension_map[i]);
            char *token;
            int count = 0;

            while ((token = strsep(&str, " ")) != NULL) {

                if (count == 0) {

                    mime = token;

                } else {

                    hash_put(mime_map, token, mime, sizeof(char*), NULL);
                }

                count++;
            }

            free(str);
        }
    }


    char *ext = strrchr(filename, '.');

    if (ext == NULL) {
        return MIME_DEFAULT;
    }

    Node *found = hash_get(mime_map, ++ext);

    if (found) {
        mime = (char*) node_value(found);
    }

    if (mime == NULL) {
        mime = MIME_DEFAULT;
    }

    return mime;
}
