/**
 * RedDwarf
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <aul.h>

#include "debug.h"
#include "mime.h"
#include "resource.h"


/**
 * Initializes a new resource structure
 */
Resource *
resource_new()
{
    Resource *res = malloc(sizeof(Resource));

    if (res == NULL) {
        return NULL;
    }

    res->ino = 0;
    res->path = NULL;
    res->hash = NULL;
    res->mtime = 0;
    res->size = 0;
    res->mime = NULL;
    res->content = str_new();

    return res;
}

/**
 * Destroys a resource structure
 */
void
resource_free(Resource *res)
{
    if (res == NULL) {
        return;
    }

    str_free(&res->content);

    free(res);
    res = NULL;
}

/**
 * Loads information about a path
 */
int
resource_stat(char *path, struct stat *buf)
{
    int ret = stat(path, buf);

    return ret == -1 ? RES_NOT_EXISTS : RES_EXISTS;
}

/**
 * Return whether a resource exists
 */
int
resource_exists(char *path)
{
    struct stat buf;
    int ret;

    ret = resource_stat(path, &buf);

    return ret == RES_EXISTS ? 1 : 0;
}

/**
 * Returns the resource hash
 */
char *
resource_get_hash(Resource *res)
{
    char *hash = NULL;
    char buff[200];

    memset(buff, 0, 200);
    sprintf(buff, "%lu-%ld-%ld", (unsigned long) res->ino, (long) res->size, res->mtime);

    hash = buff;

    LOG_DEBUG("[Hash]: %s", hash);

    return hash;
}

/**
 * Loads resource information from a path
 */
int
resource_from_path(Resource *res, char *path)
{
    if (res == NULL) {
        return RES_BAD_PARAMETER;
    }

    struct stat buf;
    FILE *fd;
    size_t fsize;
    int ret;
    char *content, *filename;

    ret = resource_stat(path, &buf);

    if (ret == RES_NOT_EXISTS) {
        LOG_DEBUG("Cannot read file '%s'", path);
        return ret;
    }

    res->ino = buf.st_ino;
    res->path = path;
    res->mtime = buf.st_mtime;
    res->hash = resource_get_hash(res);

    if (S_ISDIR(buf.st_mode)) {
        return RES_IS_DIR;
    }

    if (!S_ISREG(buf.st_mode)) {
        return RES_NOT_REG;
    }

    fsize = buf.st_size;
    fd = fopen(path, "rb");

    if (fd == NULL) {
        LOG_DEBUG("Cannot open '%s' for reading", path);
        return RES_NOT_READABLE;
    }

    content = malloc(sizeof(char) * fsize + 1);
    memset(content, 0, fsize + 1);
    fread(content, fsize, 1, fd);
    fclose(fd);

    filename = strrchr(path, '/');

    res->size = fsize;
    res->mime = mime_get_from_name(++filename);
    res->content = str_nset(res->content, content, fsize);

    free(content);

    return RES_EXISTS;
}

/**
 * Loads resource information from a string
 */
int
resource_from_string(Resource *res, char *content, size_t size, char *mime)
{
    if (res == NULL) {
        return RES_BAD_PARAMETER;
    }

    res->ino = 0;
    res->path = NULL;
    res->mtime = 0;
    res->size = size;
    res->hash = resource_get_hash(res);
    res->mime = mime;
    res->content = str_nset(res->content, content, size);

    return RES_EXISTS;
}

/**
 * Given a resource recalculate the hash and returns
 * TRUE if the differs (Resource has changed).
 */
int
resource_has_changed(Resource *res)
{
    if (res == NULL) {
        return 0;
    }

    int changed = 0;
    struct stat buf;
    char *hash;

    if (resource_stat(res->path, &buf) == RES_NOT_EXISTS) {
        LOG_DEBUG("Cannot read file '%s'", res->path);
        return RES_NOT_EXISTS;
    };

    hash = resource_get_hash(res);
    changed = strcmp(res->hash, hash) == 0 ? 0 : 1;
    free(hash);

    return changed;
}
