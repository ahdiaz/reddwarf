/**
 * RedDwarf
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#ifndef __RED_DWARF_HTTP_HEADERS_H__
#define __RED_DWARF_HTTP_HEADERS_H__


#define HTTP_HEADER_PROTOCOL          "HTTP/1.1"
#define HTTP_HEADER_CONTENT_TYPE      "Content-Type"
#define HTTP_HEADER_CONTENT_LENGTH    "Content-Length"
#define HTTP_HEADER_LOCATION          "Location"
#define HTTP_HEADER_DATE              "Date"
#define HTTP_HEADER_LAST_MODIFIED     "Last-Modified"
#define HTTP_HEADER_SERVER            "Server"
#define HTTP_HEADER_CONNECTION        "Connection"
#define HTTP_HEADER_AGE               "Age"
#define HTTP_HEADER_ALLOW             "Allow"
#define HTTP_HEADER_CACHE_CONTROL     "Cache-Control"
#define HTTP_HEADER_IF_MODIFIED_SINCE "If-Modified-Since"
#define HTTP_HEADER_REFERER           "Referer"
#define HTTP_HEADER_USER_AGENT        "User-Agent"

#endif