/**
 * RedDwarf
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#ifndef __RED_DWARF_HTTP_MIME_H__
#define __RED_DWARF_HTTP_MIME_H__


char *
mime_get_from_name(char *filename);

#endif