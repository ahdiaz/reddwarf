/**
 * RedDwarf
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#ifndef __RED_DWARF_SOCKET_H__
#define __RED_DWARF_SOCKET_H__

#define SOCKET_MAX_PENDING_REQUESTS 10
#define SOCKET_CLIENT_BUFFER_SIZE 1024

#include <netinet/in.h>

#include "settings.h"


typedef struct {

    struct sockaddr_in serv_addr;

    const char *serv_addr_str;

    struct sockaddr_in client_addr;

    const char *client_addr_str;

} ConnInfo;

/**
 * Request handler signature.
 * Clients must implement a function with this signature and pass it
 * to socket_listen().
 *
 * The returned string will be send to the socket client as response.
 *
 * @param char*
 *        The input string
 *
 * @param char**
 *        The output string
 *
 * @return size_t
 *         The response total size
 */
typedef size_t (*SocketRequestHandler)(ConnInfo, char*, char**);


void
socket_listen(SocketRequestHandler request_handler, Settings settings);

#endif
