/**
 * RedDwarf
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#ifndef __RED_DWARF_HTTP_RESPONSE_H__
#define __RED_DWARF_HTTP_RESPONSE_H__

#include <aul.h>

#include "resource.h"
#include "http-request.h"


/**
 * Stores the response information
 */
typedef struct {

    // Headers hash map
    Hash *headers;

    // The response status
    String *status;

    // Response body
    Resource *resource;

} HttpResponse;

/**
 * Initializes a new response structure
 */
HttpResponse *
http_response_new();

/**
 * Destroys a response structure
 */
void
http_response_free(HttpResponse *response);

/**
 * Returns a string (String) representation
 * ready to send back to the client
 */
String *
http_response_to_string(HttpRequest *request, HttpResponse *response);

/**
 * Gets the response status code
 */
int
http_response_get_status_code(HttpResponse *resp);

/**
 * Gets the response status
 */
char *
http_response_get_status(HttpResponse *response);

/**
 * Sets the response status
 */
void
http_response_set_status(HttpResponse *response, char *status);

/**
 * Adds a header
 */
void
http_response_add_header(HttpResponse *response, const char *header, char *value);

/**
 * Removes a header
 */
void
http_response_remove_header(HttpResponse *response, char *header);

/**
 * Generic function to build a response
 */
void
http_response_build(HttpResponse *response, char *status, Resource *resource);

/**
 * Builds a status 200 response
 */
void
http_response_build_ok(HttpResponse *response, Resource *resource);

/**
 * Builds a status 404 response
 */
void
http_response_build_notfound(HttpResponse *response);

/**
 * Builds a status 304 response
 */
void
http_response_build_notmodified(HttpResponse *response);

/**
 * Builds a status 301 response
 */
void
http_response_build_redirect(HttpResponse *response, char *location);

/**
 * Builds a status 403 response
 */
void
http_response_build_forbidden(HttpResponse *response);

/**
 * Builds a status 400 response
 */
void
http_response_build_badrequest(HttpResponse *response);

/**
 * Builds a status 405 response
 */
void
http_response_build_badmethod(HttpResponse *response);

/**
 * Builds a status 500 response
 */
void
http_response_build_server_error(HttpResponse *response);

/**
 * Builds a status 204 in response to a OPTIONS request
 */
void
http_response_build_options(HttpResponse *response);

/**
 * Builds a directory list
 */
void
http_response_build_dirlist(HttpResponse *response, char *path, char *uri);

#endif
