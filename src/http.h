/**
 * RedDwarf
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#ifndef __RED_DWARF_HTTP_HTTP_H__
#define __RED_DWARF_HTTP_HTTP_H__

#include "settings.h"

int
http_process_request(HttpRequest *request, HttpResponse **response, Settings settings);

#endif