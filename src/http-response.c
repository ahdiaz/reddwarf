/**
 * RedDwarf
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <aul.h>

#include "config.h"
#include "debug.h"
#include "http-headers.h"
#include "datetime.h"
#include "http-request.h"
#include "http-response.h"
#include "resource.h"


int
http_response_headers_iterator(Node *node, Hash *hash, Hash *new_hash, void *user_data)
{
    LOG_DEBUG("%s: %s", node->key, (char *)node->data);

    return 0;
}

void
http_response_debug_headers(HttpResponse *resp)
{
    if (resp == NULL) {
        return;
    }

    LOG_DEBUG("======== BEGIN RESPONSE HEADERS =========");
    LOG_DEBUG("%s %s", HTTP_HEADER_PROTOCOL, http_response_get_status(resp));
    hash_foreach(resp->headers, http_response_headers_iterator, NULL);
    LOG_DEBUG("======== END RESPONSE HEADERS =========");
}

// void
// http_response_debug_content(HttpResponse *resp)
// {
//     if (resp == NULL) {
//         return;
//     }
//     LOG_DEBUG("======== BEGIN RESPONSE CONTENT =========");
//     for (long z = 0; z < resp->content->size; z++) {
//         LOG_DEBUG("%c", sb_text(resp->content)[z]);
//     }
//     LOG_DEBUG("======== END RESPONSE CONTENT =========");
// }

/**
 * Initializes a new response structure
 */
HttpResponse *
http_response_new()
{
    HttpResponse *resp = malloc(sizeof(HttpResponse));

    if (resp == NULL) {
        return NULL;
    }

    resp->headers = hash_new();
    resp->status = str_new();
    resp->resource = resource_new();

    return resp;
}

/**
 * Destroys a response structure
 */
void
http_response_free(HttpResponse *resp)
{
    if (resp == NULL) {
        return;
    }

    hash_free(&resp->headers);
    str_free(&resp->status);
    resource_free(resp->resource);

    free(resp);
    resp = NULL;
}

/**
 * Common headers
 */
void
http_response_add_common_headers(HttpResponse *resp)
{
    time_t ticks;
    char buff[DATE_LEN];
    char *time_now = time_to_rfc2616(buff, time(&ticks));
    String *time_str = str_new_set(time_now);

    http_response_add_header(resp, HTTP_HEADER_SERVER, SERVER_NAME);
    http_response_add_header(resp, HTTP_HEADER_DATE, str_value(time_str));
    http_response_add_header(resp, HTTP_HEADER_CONNECTION, "keep-alive");

    str_free(&time_str);
}

/**
 * Clean headers
 */
void
http_response_clean_headers(HttpResponse *resp)
{
    if (resp != NULL) {
        hash_empty(resp->headers);
    }
}


int
http_response_headers_to_string_iterator(Node *node, Hash *hash, Hash *new_hash, void *user_data)
{
    char *header = (char*) node->key;
    char *value = (char*) node->data;
    String *s_resp = (String*) user_data;

    // LOG_DEBUG("%s: %s", header, value);

    str_append(s_resp, header);
    str_append(s_resp, ": ");
    str_append(s_resp, value);
    str_append(s_resp, "\n");

    return 0;
}

/**
 * Returns a string (String) representation
 * ready to send back to the client
 */
String *
http_response_to_string(HttpRequest *request, HttpResponse *response)
{
    if (response == NULL) {
        return NULL;
    }

    String *s_resp = str_new();

    str_append(s_resp, (char*) HTTP_HEADER_PROTOCOL);
    str_append(s_resp, " ");
    str_append(s_resp, str_value(response->status));
    str_append(s_resp, "\n");

    hash_foreach(response->headers, http_response_headers_to_string_iterator, s_resp);

    str_append(s_resp, "\n");

    if (strcmp(http_request_get_method(request), "HEAD") != 0) {
        str_nappend(s_resp, str_value(response->resource->content), str_len(response->resource->content));
    }

    http_response_debug_headers(response);

    return s_resp;
}

/**
 * Gets the response status code
 */
int
http_response_get_status_code(HttpResponse *resp)
{
    if (resp == NULL) {
        return 0;
    }

    char buff[3];
    int code;

    strncpy(buff, str_value(resp->status), 3);
    code = atoi(buff);

    return code;
}

/**
 * Gets the response status
 */
char *
http_response_get_status(HttpResponse *resp)
{
    if (resp == NULL) {
        return NULL;
    }

    return str_value(resp->status);
}

/**
 * Sets the response status
 */
void
http_response_set_status(HttpResponse *resp, char *status)
{
    if (resp != NULL) {
        str_set(resp->status, status);
    }
}

/**
 * Adds a header
 */
void
http_response_add_header(HttpResponse *resp, const char *header, char *value)
{
    if (resp != NULL) {
        hash_put(resp->headers, header, value, sizeof(Hash*), NULL);
    }
}

/**
 * Removes a header
 */
void
http_response_remove_header(HttpResponse *resp, char *header)
{
    if (resp != NULL) {
        hash_remove(resp->headers, header);
    }
}

/**
 * Generic function to build a response
 */
void
http_response_build(HttpResponse *resp, char *status, Resource *resource)
{
    if (resp == NULL) {
        return;
    }

    char buff[100];
    sprintf(buff, "%ld", resource->size);
    String *size = str_new_set(buff);

    http_response_clean_headers(resp);

    http_response_set_status(resp, status);
    http_response_add_common_headers(resp);
    http_response_add_header(resp, HTTP_HEADER_CONTENT_LENGTH, str_value(size));
    http_response_add_header(resp, HTTP_HEADER_CONTENT_TYPE, resource->mime);

    str_nset(resp->resource->content, str_value(resource->content), str_len(resource->content));
    str_free(&size);
}

/**
 * Builds a status 200 response
 */
void
http_response_build_ok(HttpResponse *resp, Resource *resource)
{
    if (resp == NULL) {
        return;
    }

    char time_buf[DATE_LEN];
    char *buff = time_to_rfc2616(time_buf, resource->mtime);
    String *mtime = str_new_set(buff);

    http_response_build(resp, "200 Ok", resource);
    http_response_add_header(resp, HTTP_HEADER_LAST_MODIFIED, str_value(mtime));
    str_free(&mtime);
}

/**
 * Builds a status 404 response
 */
void
http_response_build_notfound(HttpResponse *resp)
{
    if (resp == NULL) {
        return;
    }

    char *body = "<html><head><title>404 It's not here</title></head><body><h2>404 It's not here</h2></body></html>";
    Resource *resource;

    resource = resource_new();
    resource_from_string(resource, body, (size_t) strlen(body), "text/html");

    http_response_build(resp, "404 Not found", resource);
    resource_free(resource);
}

/**
 * Builds a status 304 response
 */
void
http_response_build_notmodified(HttpResponse *resp)
{
    if (resp == NULL) {
        return;
    }

    Resource *resource;

    resource = resource_new();
    resource_from_string(resource, "", 0, "text/html");

    http_response_build(resp, "304 Not Modified", resource);
    resource_free(resource);
}

/**
 * Builds a status 301 response
 */
void
http_response_build_redirect(HttpResponse *resp, char *location)
{
    if (resp == NULL) {
        return;
    }

    Resource *resource;

    char body[500];
    sprintf(body, "<html><head><title>301 Moved Permanently</title></head><body><h2>301 Moved Permanently</h2><a href=\"%s\">%s</a></body></html>", location, location);

    resource = resource_new();
    resource_from_string(resource, body, (size_t) strlen(body), "text/html");

    http_response_build(resp, "301 Moved Permanently", resource);
    http_response_add_header(resp, HTTP_HEADER_LOCATION, location);
    resource_free(resource);
}

/**
 * Builds a status 403 response
 */
void
http_response_build_forbidden(HttpResponse *resp)
{
    if (resp == NULL) {
        return;
    }

    char *body = "<html><head><title>403 Forbidden</title></head><body><h2>403 Forbidden</h2></body></html>";
    Resource *resource;

    resource = resource_new();
    resource_from_string(resource, body, (size_t) strlen(body), "text/html");

    http_response_build(resp, "403 Forbidden", resource);
    resource_free(resource);
}

/**
 * Builds a status 400 response
 */
void
http_response_build_badrequest(HttpResponse *resp)
{
    if (resp == NULL) {
        return;
    }

    char *body = "<html><head><title>400 Bad Request</title></head><body><h2>400 Bad Request</h2></body></html>";
    Resource *resource;

    resource = resource_new();
    resource_from_string(resource, body, (size_t) strlen(body), "text/html");

    http_response_build(resp, "400 Bad Request", resource);
    resource_free(resource);
}

/**
 * Builds a status 405 response
 */
void
http_response_build_badmethod(HttpResponse *resp)
{
    if (resp == NULL) {
        return;
    }

    char *body = "<html><head><title>405 Method Not Allowed</title></head><body><h2>405 Method Not Allowed</h2></body></html>";
    Resource *resource;

    resource = resource_new();
    resource_from_string(resource, body, (size_t) strlen(body), "text/html");

    http_response_build(resp, "405 Method Not Allowed", resource);
    http_response_add_header(resp, HTTP_HEADER_ALLOW, "OPTIONS, GET, HEAD");
    resource_free(resource);
}

/**
 * Builds a status 500 response
 */
void
http_response_build_server_error(HttpResponse *resp)
{
    if (resp == NULL) {
        return;
    }

    char *body = "<html><head><title>500 Server Error</title></head><body><h2>500 Ooops!<br/>The server have had a problem</h2></body></html>";
    Resource *resource;

    resource = resource_new();
    resource_from_string(resource, body, (size_t) strlen(body), "text/html");

    http_response_build(resp, "500 Server Error", resource);
    resource_free(resource);
}

/**
 * Builds a status 204 in response to a OPTIONS request
 */
void
http_response_build_options(HttpResponse *resp)
{
    if (resp == NULL) {
        return;
    }

    char *body = "";
    Resource *resource;

    resource = resource_new();
    resource_from_string(resource, body, 0, "text/html");

    http_response_build(resp, "204 No Content", resource);
    http_response_add_header(resp, HTTP_HEADER_ALLOW, "OPTIONS, GET, HEAD");
    resource_free(resource);
}

void
_list_free_string_node(void *str)
{
    String *s = (String*) str;
    str_free(&s);
}

/**
 * Builds a directory list
 */
void
http_response_build_dirlist(HttpResponse *resp, char *path, char *uri)
{
    if (resp == NULL) {
        return;
    }

    DIR *dir = NULL;
    struct dirent *ent;
    List *list = NULL;
    String *currname = NULL;

    dir = opendir(path);

    if (dir == NULL) {
        return;
    }

    list = list_new_ff(_list_free_string_node);
    currname = str_new();

    while ((ent = readdir(dir)) != NULL) {

        struct stat s;

        // char d_name[]
        if ((ent->d_name[0] == '.') && (ent->d_name[1] == '\0')) {
            continue;
        }

        str_set(currname, path);
        str_append(currname, ent->d_name);

        if (stat(str_value(currname), &s) == -1) {
            continue;
        }

        String *buf = str_new_set(ent->d_name);
        list_append(list, buf, str_len(buf), NULL);
    }

    closedir(dir);
    // qsort(list, entries, sizeof(struct dlent*), dlent_cmp);
    // list_sort(list);


    String *body = NULL, *newuri = NULL;
    Node *item = NULL;
    Resource *resource = NULL;

    body = str_new();
    newuri = str_new();

    str_append(body, "<html><head><title>");
    str_append(body, uri);
    str_append(body, "</title></head><body><h2>Directory listing of ");
    str_append(body, uri);
    str_append(body, "</h2><pre>");

    item = list_first(list);

    while (item) {

        String *fileitem = (String*) node_value(item);

        newuri = str_set(newuri, uri);
        // newuri = str_append(newuri, "/");
        newuri = str_append(newuri, str_value(fileitem));

        str_append(body, "<a href=\"");
        str_append(body, str_value(newuri));
        str_append(body, "\">");
        str_append(body, str_value(fileitem));
        str_append(body, "</a><br/>");

        item = list_next(item);
    }

    str_append(body, "</pre></body></html>");

    resource = resource_new();
    resource_from_string(resource, str_value(body), str_len(body), "text/html");

    http_response_build(resp, "200 Ok", resource);

    resource_free(resource);
    list_free(&list);
    str_free(&body);
    str_free(&newuri);
    str_free(&currname);
}
