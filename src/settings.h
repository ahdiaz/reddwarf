/**
 * RedDwarf
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#ifndef __RED_DWARF_HTTP_SETTINGS_H__
#define __RED_DWARF_HTTP_SETTINGS_H__

#include <aul.h>

// Resource description
typedef struct {

    // Address to bind to
    String *address;

    // Listening port
    unsigned int port;

    // WWW root directory
    String *wwwroot;

    // Default index file
    String *index;

    // Return directory listings
    int listings;

} Settings;

/**
 * Initializes a new settings structure
 */
Settings
settings_init(int argc, char *argv[]);

/**
 * Returns the settings instance, previously
 * initialized by a call to settings_init()
 */
Settings
settings_get();

/**
 * Display command line help
 */
void
settings_print_help();

/**
 * Display program version
 */
void
settings_print_version();

/**
 * Prints the current configuration
 */
void
settings_debug();

#endif
