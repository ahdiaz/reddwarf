/**
 * RedDwarf
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#define _XOPEN_SOURCE // http://man7.org/linux/man-pages/man3/strptime.3.html
#include <time.h>

#include "datetime.h"


char *
time_to_string(char *dest, const time_t when, char *format)
{
    time_t when_copy = when;
    struct tm *gmt_tm;

    // Convert the time zone to GMT
    gmt_tm = gmtime(&when_copy);
    // Request that mktime() looksup dst in the timezone database
    gmt_tm->tm_isdst = -1;
    // And call mktime so the dst is refreshed, otherwise will be empty
    mktime(gmt_tm);

    strftime(dest, DATE_LEN, format, gmt_tm);

    return dest;
}

/**
 * Returns a representation of the time t_time according
 * to rfc2616: http://www.w3.org/Protocols/rfc2616/rfc2616-sec3.html#sec3.3
 * ex: Sun, 06 Nov 1994 08:49:37 GMT
 */
char *
time_to_rfc2616(char *dest, const time_t when)
{
    return time_to_string(dest, when, DATE_FORMAT_RFC2616);
}

time_t
rfc2616_to_time(char *date)
{
    struct tm tm = {0, 0, 0, 0, 0, 0, 0, 0, 0};
    time_t when;

    strptime(date, DATE_FORMAT_RFC2616, &tm);
    when = mktime(&tm);

    return when;
}

char *
time_to_log(char *dest, const time_t when)
{
    return time_to_string(dest, when, DATE_FORMAT_LOG);
}

int
datecmp(time_t date1, time_t date2)
{
    double diff_t = difftime(date1, date2);

    return (int) diff_t;
}

int
datestrcmp(char *date1, char *date2)
{
    time_t time1, time2;

    time1 = rfc2616_to_time(date1);
    time2 = rfc2616_to_time(date2);

    return datecmp(time1, time2);
}
