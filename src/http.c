/**
 * RedDwarf
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#include <string.h>
#include <aul.h>

#include "config.h"
#include "debug.h"
#include "http-headers.h"
#include "datetime.h"
#include "settings.h"
#include "http-request.h"
#include "http-response.h"
#include "resource.h"


void
http_handle_request(HttpRequest *request, HttpResponse **response, Settings settings);

int
http_handle_options(HttpRequest *request, HttpResponse **response)
{
    char *method;

    method = http_request_get_method(request);

    if (strcmp(method, "OPTIONS") == 0) {

        http_response_build_options(*response);

        return 1;
    }

    return 0;
}

int
http_handle_file(int fileinfo, HttpRequest *request, HttpResponse **response, Resource *resource, Settings settings)
{
    if (fileinfo != RES_EXISTS) {
        return 0;
    }

    char *lastmod = NULL;
    char *ifmodsince = http_request_get_header(request, HTTP_HEADER_IF_MODIFIED_SINCE);

    if (ifmodsince != NULL) {

        char date[DATE_LEN];
        lastmod = time_to_rfc2616(date, resource->mtime);
    }

    if (lastmod && datestrcmp(ifmodsince, lastmod) >= 0) {

        http_response_build_notmodified(*response);

    } else {

        http_response_build_ok(*response, resource);
    }

    return 1;
}

int
http_handle_dir(int fileinfo, HttpRequest *request, HttpResponse **response, Resource *resource, Settings settings)
{
    if (fileinfo != RES_IS_DIR) {
        return 0;
    }

    char *uri = http_request_get_uri(request);
    int last_pos = strlen(uri) - 1;
    String *location = str_new_set(uri);

    if (uri[last_pos] != '/') {

        str_append(location, "/");
        http_response_build_redirect(*response, str_value(location));

    } else {

        String *check_index = str_new_set(resource->path);
        str_append(check_index, str_value(settings.index));

        if (resource_exists(str_value(check_index))) {

            str_append(location, str_value(settings.index));
            str_nset(request->uri, str_value(location), str_len(location));

            http_handle_request(request, response, settings);

        } else if (settings.listings) {

            http_response_build_dirlist(*response, resource->path, uri);

        } else {

            http_response_build_forbidden(*response);
        }

        str_free(&check_index);
    }

    free(uri);
    str_free(&location);

    return 1;
}

int
http_handle_error(int fileinfo, HttpRequest *request, HttpResponse **response, Settings settings)
{
    if (http_request_get_parse_error(request) == HTTP_REQUEST_ERROR_METHOD) {

        http_response_build_badmethod(*response);

    } else if (http_request_get_parse_error(request) != HTTP_REQUEST_ERROR_NONE) {

        http_response_build_badrequest(*response);

    } else if (fileinfo == RES_NOT_READABLE || fileinfo == RES_NOT_REG) {

        http_response_build_forbidden(*response);

    } else if (fileinfo == RES_NOT_EXISTS) {

        http_response_build_notfound(*response);

    } else {

        return 0;
    }

    return 1;
}

void
http_handle_request(HttpRequest *request, HttpResponse **response, Settings settings)
{
    Resource *resource = NULL;
    String *resource_path = NULL;
    char *path = NULL;
    char *uri = NULL;
    int fileinfo, handled;

    uri = http_request_get_uri(request);

    resource_path = str_new();
    str_append(resource_path, str_value(settings.wwwroot));
    str_append(resource_path, uri);

    path = str_value(resource_path);

    char *aux = NULL;
    char *p = path;
    int i = 0;

    // Replace double slashes by a single one
    while (*p != 0) {
        if (*(p - 1) == '/' && *p == '/') {
            aux = calloc(strlen(path), sizeof(char));
            memset(aux, 0, strlen(path));
            memcpy(aux, path, i);
            memcpy(aux + i, path + i + 1, strlen(path) - i);
            break;
        }
        p++;
        i++;
    }

    str_set(resource_path, aux);
    path = str_value(resource_path);

    LOG_DEBUG("Requesting URI %s", uri);
    LOG_DEBUG("Trying to serve %s", path);

    resource = resource_new();
    fileinfo = resource_from_path(resource, path);


    handled = http_handle_options(request, response);

    if (!handled) {
        handled = http_handle_error(fileinfo, request, response, settings);
    }

    if (!handled) {
        handled = http_handle_file(fileinfo, request, response, resource, settings);
    }

    if (!handled) {
        handled = http_handle_dir(fileinfo, request, response, resource, settings);
    }

    resource_free(resource);
    str_free(&resource_path);
    free(path);
}

int
http_process_request(HttpRequest *request, HttpResponse **response, Settings settings)
{
    if (request == NULL) {
        return 1;
    }

    if (*response == NULL) {
        *response = http_response_new();
    }

    http_handle_request(request, response, settings);

    return 0;
}