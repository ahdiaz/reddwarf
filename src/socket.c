/**
 * RedDwarf
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <aul.h>

#include "debug.h"
#include "settings.h"
#include "socket.h"


/**
 * Just display a message and die.
 *
 * @param char* msg
 */
void die(char *msg)
{
    perror(msg);
    exit(1);
}

/**
 * Handles the client connection. Reads the request and pass it to
 * the request_handler() function, this must return a response to send back.
 *
 * @param int connfd
 *        The connection identifier.
 *
 * @param SocketRequestHandler request_handler
 *        This function must handle the request and return a response.
 */
void socket_handle_client(int connfd, SocketRequestHandler request_handler, struct sockaddr_in serv_addr, struct sockaddr_in client_addr)
{
    int received_size = 0;
    time_t start_t, end_t;
    double processed;
    char received_buffer[SOCKET_CLIENT_BUFFER_SIZE];
    char *response = NULL;
    size_t resp_size;
    String *request = str_new();
    ConnInfo conninfo;

    conninfo.serv_addr = serv_addr;
    conninfo.serv_addr_str = inet_ntoa(serv_addr.sin_addr);
    conninfo.client_addr = client_addr;
    conninfo.client_addr_str = inet_ntoa(client_addr.sin_addr);

    start_t = time(&start_t);

    do {

        memset(received_buffer, 0, SOCKET_CLIENT_BUFFER_SIZE);

        received_size = (int) recv(connfd, received_buffer, SOCKET_CLIENT_BUFFER_SIZE, 0);

        if (received_size >= 0 ) {
            request = str_nappend(request, received_buffer, received_size);
        }

    } while (received_size == SOCKET_CLIENT_BUFFER_SIZE);


    if (str_len(request) > 0) {

        resp_size = request_handler(conninfo, str_value(request), &response);

        ssize_t sent = send(connfd, response, resp_size, MSG_DONTWAIT);
        LOG_DEBUG("Response bytes: %lu, Sent bytes: %lu", resp_size, sent);

        if (sent != resp_size) {
            LOG_WARN("[SocketClient]: Sent data size (%lu) differs from the response total size (%lu).\n", resp_size, sent);
        }
    }


    end_t = time(&end_t);
    processed = difftime(end_t, start_t);
    LOG_DEBUG("Content served in %f seconds", processed);

    str_free(&request);
    shutdown(connfd, SHUT_RDWR);
}

/**
 * Initializes a socket and start to listen for connections.
 *
 * @param SocketRequestHandler request_handler
 *        This function will be passed to socket_handle_client()
 */
void socket_listen(SocketRequestHandler request_handler, Settings settings)
{
    int listenfd = 0, connfd = 0;
    struct sockaddr_in serv_addr, client_addr;
    socklen_t serv_addr_len;

    memset(&serv_addr, 0, sizeof(serv_addr));
    memset(&client_addr, 0, sizeof(client_addr));

    if ((listenfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        die("Failed to create socket");
    }

    int option = 1;
    setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = settings.address ? inet_addr(str_value(settings.address)) : htonl(INADDR_ANY);
    serv_addr.sin_port = htons(settings.port);

    serv_addr_len = sizeof(serv_addr);
    if ((bind(listenfd, (struct sockaddr*) &serv_addr, serv_addr_len)) < 0) {
        die("Failed to bind the server socket");
    }

    if (listen(listenfd, SOCKET_MAX_PENDING_REQUESTS) < 0) {
        die("Failed to listen on server socket");
    }

    serv_addr_len = sizeof(serv_addr);
    if (getsockname(listenfd, (struct sockaddr *)&serv_addr, &serv_addr_len) == -1) {
        LOG_ERROR("getsockname()");
    }

    // *(const struct in_addr *) &serv_addr.sin_addr
    const char *str_addr = inet_ntoa(serv_addr.sin_addr);
    LOG_INFO("Server bound and listening on http://%s:%u", str_addr, settings.port);
    LOG_INFO("Serving content from %s", settings.wwwroot->str);

    while (1) {

        unsigned int clientlen = sizeof(client_addr);

        if ((connfd = accept(listenfd, (struct sockaddr*) &client_addr, &clientlen)) < 0) {

            perror("Failed to accept client connection");

        } else {

            const char *str_addr = inet_ntoa(client_addr.sin_addr);
            LOG_DEBUG("Accepted connection from %s", str_addr);

            socket_handle_client(connfd, request_handler, serv_addr, client_addr);
        }
    }

    shutdown(listenfd, SHUT_RDWR);
}
