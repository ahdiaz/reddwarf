/**
 * RedDwarf
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#ifndef __RED_DWARF_HTTP_REQUEST_H__
#define __RED_DWARF_HTTP_REQUEST_H__

#include <aul.h>

// Error codes
enum HttpRequestErrors {

    // Everything Ok
    HTTP_REQUEST_ERROR_NONE,

    // An error parsing the request
    HTTP_REQUEST_ERROR_PARSING,

    // Bad method
    HTTP_REQUEST_ERROR_METHOD,

    // Bad requested URI
    HTTP_REQUEST_ERROR_URI,

    // Bad protocol
    HTTP_REQUEST_ERROR_PROTOCOL
};

/**
 * Stores request information, protocol, method,
 * parsed headers...
 */
typedef struct {

    // Parse error
    int error;

    // Headers hash map
    Hash *headers;

    // Request protocol
    String *protocol;

    // Request method
    String *method;

    // Request URI
    String *uri;

    // Raw content
    String *content;

} HttpRequest;


/**
 * Initializes a request structure
 */
HttpRequest *
http_request_new();

/**
 * Destroys a request structure
 */
void
http_request_free(HttpRequest *req);

/**
 * Process a raw content and fills the HttpRequest structure
 * with the different components of the request.
 *
 * @return HttpRequest
 */
HttpRequest *
http_request_parse(HttpRequest *req, char *raw, unsigned int *error);

/**
 * Process a raw content and fills the HttpRequest structure
 * with the different components of the request.
 *
 * @return HttpRequest
 */
HttpRequest *
http_request_parse_from_string(HttpRequest *req, String *raw, unsigned int *error);

/**
 * Returns the protocol
 */
char *
http_request_get_protocol(HttpRequest *req);

/**
 * Returns the method
 */
char *
http_request_get_method(HttpRequest *req);

/**
 * Returns the rquested URI
 */
char *
http_request_get_uri(HttpRequest *req);

/**
 * Returns the raw request content
 */
char *
http_request_get_raw(HttpRequest *req);

/**
 * Returns the number of headers
 */
size_t
http_request_headers_count(HttpRequest *req);

/**
 * Returns a header value
 */
char *
http_request_get_header(HttpRequest *req, const char *key);

/**
 * Returns the parse error
 */
unsigned int
http_request_get_parse_error(HttpRequest *req);

/**
 * Prints the parsed request
 */
void
http_request_debug(HttpRequest *req);

#endif
