/**
 * RedDwarf
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#ifndef __RED_DWARF_HTTP_RESOURCE_H__
#define __RED_DWARF_HTTP_RESOURCE_H__

#include <sys/stat.h>
#include <aul.h>

#ifndef _POSIX_SOURCE
#include <time.h>
#ifndef st_atime
    #define st_atime st_atimespec.tv_sec;  /* time of last access */
#endif
#ifndef st_mtime
    #define st_mtime st_mtimespec.tv_sec;  /* time of last data modification */
#endif
#ifndef st_ctime
    #define st_ctime st_ctimespec.tv_sec;  /* time of last file status change */
#endif
#endif


// Error codes
enum RDResourceErrors {

    RES_EXISTS,

    RES_NOT_EXISTS,

    RES_NOT_READABLE,

    RES_NOT_REG,

    RES_IS_DIR,

    RES_CHANGED,

    RES_BAD_PARAMETER
};

// Resource description
typedef struct {

    // inode number
    ino_t ino;

    // Resource path
    char *path;

    // Resource hash (ino, size, mtime)
    char *hash;

    // Last modification time
    time_t mtime;

    // Resource size
    size_t size;

    // Mime type
    char *mime;

    // Resource content
    String *content;

} Resource;

/**
 * Initializes a new resource structure
 */
Resource *
resource_new();

/**
 * Destroys a resource structure
 */
void
resource_free(Resource *res);

/**
 * Return whether a resource exists
 */
int
resource_exists(char *path);

/**
 * Loads resource information from a path
 */
int
resource_from_path(Resource *res, char *path);

/**
 * Loads resource information from a string
 */
int
resource_from_string(Resource *res, char *content, size_t size, char *mime);

/**
 * Returns the resource hash
 */
char *
resource_get_hash(Resource *res);

/**
 * Given a resource recalculate the hash and returns
 * TRUE if the differs (Resource has changed).
 */
int
resource_has_changed(Resource *res);

#endif
