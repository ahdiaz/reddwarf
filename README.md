## RedDwarf HTTP server

This is a small and simple HTTP server, written mainly for fun and learning. It only serve static files, but is somehow well featured:

- Sanitization of URLs.
- Understand the HTTP methods ***GET***, ***HEAD*** and ***OPTIONS***.
- Honours the header ***If-Modified-Since***.
- Optional directory listing.
- Decent set of response statuses.
- Decent set of mime types based on file extensions.
- Fast.

By no means it is free of bugs, and no one would like to have it in a production environment. But with it is possible to quickly set up a static server in case you need it temporarily.

### Compilation and use

It only needs one dependency since I used [libaul](https://gitlab.com/ahdiaz/libaul) in this project. The program accepts some parameters in the command line. Be sure to check the help.

    $ ./configure [--prefix=/path]
    $ make
    $ make test
    $ make install

    $ reddwarf -h

    Usage: reddwarf [options]

        -a --address:   IP address to bind to
        -p --port:      Port to listen on
        -r --root:      Path to the home directory
        -i --index:     Default index file
        --no-listings:  Don't return directory listings
        -h --help:      Displays this help and exit
        -v --version:   Displays the program version and exit

    $ reddwarf -r /var/www

    *******************
    *  RedDwarf v0.1  *
    *******************

    Server bound and listening on http://127.0.0.1:8080
    Serving content from /var/www
